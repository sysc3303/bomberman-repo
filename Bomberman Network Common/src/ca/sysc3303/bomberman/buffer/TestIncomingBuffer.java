package ca.sysc3303.bomberman.buffer;

import ca.sysc3303.bomberman.network.omar.*;

public class TestIncomingBuffer {
	//************TESTING*******************///
	//**************************************///
	static IncomingBuffer b = IncomingBuffer.getInstance();
	static int i = 0;
	static int PRODUCER_DELAY_TIME = 100;
	static int CONUSMER_DELAY_TIME = 100;
	static int CONUSMER_INITIAL_DELAY = 1000;
	static int PRODUCER_INITIAL_DELAY = 100;
	
	public static void main(String argv[])
	{
		//Consumer
				(new Thread(new Runnable () {
					@Override
					public void run(){
						System.out.println("Consumer: sleeping for " + CONUSMER_INITIAL_DELAY + "before starting thread");
						try {
							Thread.sleep(CONUSMER_INITIAL_DELAY);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						while(true){
							BufferedCommand bc = b.consume();
							
							System.out.println("Consumed " + bc.getPlayerId() + " at " + System.currentTimeMillis());
							
							try {
								Thread.sleep(CONUSMER_DELAY_TIME);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				})).start();
				///////////////////

			//Producer
					(new Thread(new Runnable () {
						@Override
						public void run(){
							System.out.println("Producer: sleeping for " + PRODUCER_INITIAL_DELAY + "before starting thread");
							try {
								Thread.sleep(PRODUCER_INITIAL_DELAY);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							while(true){
								BufferedCommand bc = new BufferedCommand(i++, Deploy.BOMB);
								b.produce(bc);
								System.out.println("Produced " + bc.getPlayerId() + " at " + System.currentTimeMillis());
								try {
									Thread.sleep(PRODUCER_DELAY_TIME);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					})).start();
	}	
}//End Test
