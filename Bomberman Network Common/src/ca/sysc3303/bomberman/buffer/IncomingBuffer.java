package ca.sysc3303.bomberman.buffer;

import ca.sysc3303.bomberman.network.omar.BufferedCommand;
import ca.sysc3303.bomberman.network.omar.Deploy;

/**
 * Simple Singleton (ie BADASS!!) buffer implementation
 * Default size is 1024
 * @author Omar Samhan
 *
 */
public class IncomingBuffer {
	
	private BufferedCommand []buffer;
	private int pIndex;
	private int cIndex;
	private int Count;
	private int size;
	private static final int DEFAULT_SIZE = 1024;
	private static IncomingBuffer instance = null;

	/**
	 * 
	 * @param size
	 */
	private IncomingBuffer(int size){
		this.cIndex = this.pIndex = this.Count = 0;
		this.size = size;
		buffer = new BufferedCommand[size];
	}

	/**
	 * Default size is 1024
	 */
	private IncomingBuffer(){
		this(IncomingBuffer.DEFAULT_SIZE);
	}

	/**
	 * Blocks the calling thread if the buffer is full
	 * @param bF The buffered command to produce to the buffer
	 */
	public synchronized void produce(BufferedCommand bF) 
	{
		while(Count == size)
		{
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		buffer[pIndex] = bF;
		pIndex = incIndex(pIndex);
		Count++;
		notifyAll();
	}

	/**
	 * Blocks the calling thread if the buffer is empty
	 * @return
	 */
	public synchronized BufferedCommand consume()
	{
		BufferedCommand bF;

		while(Count == 0)
		{
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		bF = buffer[cIndex];
		cIndex = incIndex(cIndex);
		Count--;
		notifyAll();
		return bF;
	}
	
	/**
	 * Returns an IncommingBuffer with default size of 1024
	 * @return
	 */
	public static synchronized IncomingBuffer getInstance()
	{
		if(instance == null) instance = new IncomingBuffer();
		return instance;
	}
	
	private int incIndex(int x)
	{
		if(x == size-1) return 0;
		else return ++x;
	}
}
