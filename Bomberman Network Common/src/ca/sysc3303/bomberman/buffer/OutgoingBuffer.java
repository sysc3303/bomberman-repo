package ca.sysc3303.bomberman.buffer;


/**
 * Simple Singleton (ie BADASS!!) buffer implementation
 * Default size is 1024
 * @author Omar Samhan
 *
 */
public class OutgoingBuffer {
	
	private String[] buffer;
	private int pIndex;
	private int cIndex;
	private int Count;
	private int size;
	private static final int DEFAULT_SIZE = 1024;
	private static OutgoingBuffer instance = null;

	/**
	 * 
	 * @param size
	 */
	public OutgoingBuffer(int size){
		this.cIndex = this.pIndex = this.Count = 0;
		this.size = size;
		buffer = new String[size];
	}

	/**
	 * Default size is 1024
	 */
	private OutgoingBuffer(){
		this(OutgoingBuffer.DEFAULT_SIZE);
	}

	/**
	 * Blocks the calling thread if the buffer is full
	 * @param bF The buffered command to produce to the buffer
	 */
	public synchronized void produce(String data) 
	{
		while(Count == size)
		{
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		buffer[pIndex] = data;
		pIndex = incIndex(pIndex);
		Count++;
		notifyAll();
	}

	/**
	 * Blocks the calling thread if the buffer is empty
	 * @return
	 */
	public synchronized String consume()
	{
		String data;

		while(Count == 0)
		{
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		data = buffer[cIndex];
		cIndex = incIndex(cIndex);
		Count--;
		notifyAll();
		return data;
	}
	
	/**
	 * Returns an IncommingBuffer with default size of 1024
	 * @return
	 */
	public static synchronized OutgoingBuffer getInstance()
	{
		if(instance == null) instance = new OutgoingBuffer();
		return instance;
	}
	
	private int incIndex(int x)
	{
		if(x == size-1) return 0;
		else return ++x;
	}
}
