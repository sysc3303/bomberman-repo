package ca.sysc3303.bomberman.network.omar;

/**
 * Types of Commands
 * @author Omar Samhan
 *
 */
public enum CommandType {
	DEPLOY, GAMESTATE, MOVEOBJ
}
