package ca.sysc3303.bomberman.network.omar;

/**
 * Types of Deploy
 * @author Omar Samhan
 *
 */
public enum Deploy{
	BOMB, DELAYED_BOMB, DETONATE, NA
}
