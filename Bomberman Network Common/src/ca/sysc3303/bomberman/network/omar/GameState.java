package ca.sysc3303.bomberman.network.omar;

/**
 * A command to set the state for the client in the game (example: start game, end game...)
 * @author Omar Samhan
 *
 */
public enum GameState{
	START_GAME, END_GAME, JOIN_GAME, RESET_LIFE, NA
}