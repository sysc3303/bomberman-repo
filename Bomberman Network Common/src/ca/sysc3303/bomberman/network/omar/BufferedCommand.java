package ca.sysc3303.bomberman.network.omar;

/**
 * The Commands to shove into IncommingBuffer
 * @author Omar Samhan
 *
 */
public class BufferedCommand implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Deploy deploy;
	private GameState game;
	private MoveObj move;
	private int playerId;
	
	/**
	 * 
	 * @param playerId
	 */
	public BufferedCommand(int playerId){
		this.playerId = playerId;
		deploy = Deploy.NA;
		game = GameState.NA;
		move = MoveObj.NA;
	}
	
	/**
	 * 
	 * @param playerId
	 * @param move
	 */
	public BufferedCommand(int playerId, MoveObj move){
		this(playerId);
		this.move = move;
	}
	
	/**
	 * 
	 * @param playerId
	 * @param deploy
	 */
	public BufferedCommand(int playerId, Deploy deploy){
		this(playerId);
		this.deploy = deploy;
	}
	
	/**
	 * 
	 * @param playerId
	 * @param game
	 */
	public BufferedCommand(int playerId, GameState game){
		this(playerId);
		this.game = game;
	}
	
	/**
	 * 
	 */
	public BufferedCommand(){
		this(-1);
	}
	
	
	/**
	 * 
	 * @return CommandType
	 */
	public CommandType getCommandType(){
		if(deploy != Deploy.NA) return CommandType.DEPLOY;
		else if (game != GameState.NA) return CommandType.GAMESTATE;
		else return CommandType.MOVEOBJ;
	}

	/**
	 * 
	 * @return
	 */
	public int getPlayerId() {
		return playerId;
	}

	/**
	 * 
	 * @param playerId
	 */
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	/**
	 * 
	 * @return
	 */
	public Deploy getDeploy() {
		return deploy;
	}

	/**
	 * 
	 * @param deploy
	 */
	public void setDeploy(Deploy deploy) {
		this.game = GameState.NA;
		this.move = MoveObj.NA;
		this.deploy = deploy;
	}

	/**
	 * 
	 * @return
	 */
	public GameState getGame() {
		return game;
	}

	/**
	 * 
	 * @param game
	 */
	public void setGame(GameState game) {
		this.deploy = Deploy.NA;
		this.move = MoveObj.NA;
		this.game = game;
	}

	/**
	 * 
	 * @return
	 */
	public MoveObj getMove() {
		return move;
	}

	/**
	 * 
	 * @param move
	 */
	public void setMove(MoveObj move) {
		this.game = GameState.NA;
		this.deploy = Deploy.NA;
		this.move = move;
	}
}
