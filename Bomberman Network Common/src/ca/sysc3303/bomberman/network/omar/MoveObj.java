package ca.sysc3303.bomberman.network.omar;

/**
 * The movement of the player
 * @author Omar Samhan
 *
 */
public enum MoveObj{
	UP, DOWN, LEFT, RIGHT, NA
}