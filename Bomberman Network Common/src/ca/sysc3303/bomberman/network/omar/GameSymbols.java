package ca.sysc3303.bomberman.network.omar;

public class GameSymbols {
	public static char PLAYER = 'p';
	public static char BOX = 'b';
	public static char POWERUP = 'u';
	public static char EMPTY = 'e';
	/**
	 * Property Delimiter
	 */
	public static char DELPROP = ',';
	/**
	 * Tile Delimiter
	 */
	public static char DELTILE = ';';
	/**
	 * Row Delimiter
	 */
	public static char DELROW = '$';
	/**
	 * End Delimiter
	 */
	public static char DELEND = '@';
}
