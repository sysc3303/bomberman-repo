package ca.sysc3303.bomberman.server.networking;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ca.sysc3303.bomberman.buffer.IncomingBuffer;
import ca.sysc3303.bomberman.buffer.OutgoingBuffer;
import ca.sysc3303.bomberman.network.omar.*;
import ca.sysc3303.bomberman.server.Server;
import ca.sysc3303.bomberman.network.omar.BufferedCommand;
import ca.sysc3303.bomberman.network.omar.GameState;
import ca.sysc3303.bomberman.network.omar.MoveObj;

/**
 * TODO: Should deploy a thread/runnable/callable whatever needed to handle requests
 *  
 * @author Haydar
 *
 */

public class NetworkManager {

	private String clientIP;
	private InetAddress clientAddr;
	private int clientPort;
	private int incomingPort;
	private DatagramSocket incomingSocket;
	private DatagramSocket outgoingSocket;

	private InputThread inThread;
	private OutputThread outThread;
	
	// MILESTONE 2: will need this later when clients with different IP's are connected
	// private Map<InetAddress, Integer> addressMap; 
	
	public NetworkManager(int incomingPort, IncomingBuffer inBuffer, OutgoingBuffer outBuffer) {
		this.incomingPort = incomingPort;
		
		// MILESTONE 2
		// this.addressMap = new HashMap<>()

		try {
			this.incomingSocket= new DatagramSocket(this.incomingPort);
		} catch (SocketException e) {
			e.printStackTrace();
		}
		
		this.clientPort = 5001;
		
		//MILESTONE 2: fix hardcoded address
		this.clientIP = "localhost";
		
		try {
			this.clientAddr = InetAddress.getByName(clientIP);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		try {
			this.outgoingSocket = new DatagramSocket();
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	public void setup(){
		
		inThread = new InputThread();
		outThread = new OutputThread();

		inThread.start();
		outThread.start();
	}

	class InputThread extends Thread{

		private IncomingBuffer inBuffer;
		private byte[] recvData;
		private DatagramPacket recvPacket;
		private BufferedCommand command;

		public InputThread(){
			this.inBuffer = IncomingBuffer.getInstance();
			recvData = new byte[1024];
			recvPacket = new DatagramPacket(recvData, recvData.length);
			command = null;
		}

		public void run(){
			while(true){
				boolean newPlayer = false;	
				try {
					incomingSocket.receive(recvPacket);					
				} catch (IOException e) {
					e.printStackTrace();
				}
				command = parsePacket(recvPacket);
								
				//TESTING
				Server.log.produce("RECEIVED: " + printCommand(command));

				inBuffer.produce(command);
			}

		}

		private BufferedCommand parsePacket(DatagramPacket datagram){
			byte[] recvData = datagram.getData();
			ByteArrayInputStream b = null;
			ObjectInputStream o = null;
			BufferedCommand command = null;

			//de-serialize the packet data
			b = new ByteArrayInputStream(recvData);

			try {
				o = new ObjectInputStream(b);
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				command = (BufferedCommand)(o.readObject());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return command;
		}

		//TESTING
		private String printCommand(BufferedCommand command){
			int playerID = command.getPlayerId();
			MoveObj move = command.getMove();
			GameState gameState = command.getGame();
			String moveString = null;
			String gameStateString = null;

			if (move == MoveObj.UP){
				moveString = "Up";
			} else if (move == MoveObj.DOWN){
				moveString = "Down";
			} else if (move == MoveObj.LEFT){
				moveString = "Left";
			} else if (move == MoveObj.RIGHT){
				moveString = "Right";
			} else {
				moveString = "None";
			}

			if (gameState == GameState.START_GAME){
				gameStateString = "Start";
			} else if (gameState == GameState.END_GAME){
				gameStateString = "End";
			} else if (gameState == GameState.JOIN_GAME){
				gameStateString = "Join";
			} else if (gameState == GameState.RESET_LIFE){
				gameStateString = "Reset Life";
			} else if (gameState == GameState.NA){
				gameStateString = "NA";
			} else {
				gameStateString = "None";
			}

			return ("playerID = " + Integer.toString(playerID) + ", Move = "  + moveString +
					", GameState = " + gameStateString);
		}
		
		//These commented-out methods are for MILESTONE 2
		/*
		 private int getPlayerIDFromMap(InetAddress ipAddress)

		{
			int playerID=0;
			for (Map.Entry<InetAddress, Integer> entry : addressMap.entrySet()) {
				if(entry.getKey().equals(ipAddress))
				{
					playerID = entry.getValue();
				}
			}
			return playerID;
		}
		private boolean addToMap (InetAddress ipAddress)
		{
			boolean status = false;
			int playerID;
			int lastPlayerID =0;;
			if(addressMap.isEmpty())
			{
				playerID = 1;
				addressMap.put(ipAddress, playerID );
				status = true;
				return status;
			}
			else if(addressMap.containsKey(ipAddress))return false;
			else{
				for (Integer id : addressMap.values()) {
					lastPlayerID = id.intValue();
				}
				playerID = lastPlayerID + 1;
				addressMap.put(ipAddress, playerID );
				status = true;
				return status;
			}
		}
		*/
		 
		/*
		private void removeFromMap(int playerID)

		{
			Iterator it = addressMap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry)it.next();
				if(((Integer)pairs.getValue()).compareTo(playerID)==0) it.remove(); // avoids a ConcurrentModificationException
			}
		}
		*/
		
	}

	class OutputThread extends Thread{

		private OutgoingBuffer outBuffer;
		String sendData;			
		DatagramPacket sendPacket;

		public OutputThread(){
			outBuffer = OutgoingBuffer.getInstance();
			sendData = null;
			sendPacket = null;
		}

		public void run(){
			
			while(true){
				sendData = outBuffer.consume();
				
				//TESTING
				//Server.log.produce("SENDING");

				//form packet with string representation of the game
				sendPacket = buildPacket(sendData);

				try {
					outgoingSocket.send(sendPacket);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		private DatagramPacket buildPacket(String data){
			byte [] sendData = null;
			sendData = data.getBytes(); 
			DatagramPacket sendPacket = new DatagramPacket(sendData,sendData.length, clientAddr, clientPort);
			return sendPacket;
		}
	}
}
