package ca.sysc3303.bomberman.server.game.controller;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import ca.sysc3303.bomberman.buffer.IncomingBuffer;
import ca.sysc3303.bomberman.buffer.OutgoingBuffer;
import ca.sysc3303.bomberman.network.omar.BufferedCommand;
import ca.sysc3303.bomberman.server.Server;
import ca.sysc3303.bomberman.server.model.GameModel;
import ca.sysc3303.bomberman.server.model.Tile;

/**
 * Handle game controls such as creating game instance and keep reference to game model objects
 * This can be extended to control multiple game hosting
 * The event dispatcher model can be used to handle multiple events source
 * @author Haydar
 *
 */
public class GameController implements Runnable {

	private GameModel gameModel;
	private IncomingBuffer incomingBuffer;
	private OutgoingBuffer outgoingBuffer;
	private boolean running;
	private GameLoopThread gameLoop;
	private boolean gameStarted;

	public GameController(IncomingBuffer incomingBuffer, OutgoingBuffer outgoingBuffer, File mapFile)
	{
		gameModel = new GameModel(4, mapFile);

		try {
			gameModel.initGameModel();
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.incomingBuffer = IncomingBuffer.getInstance();
		this.outgoingBuffer = OutgoingBuffer.getInstance();
		running = true;
		gameStarted = false;
	}

	/**
	 * The game controller thread job is to handle incoming requests
	 * from the network manager. Invoking appropriate API on the game model.
	 * Any game control logic should be here, such as starting game, 
	 */
	@Override
	public void run() {
		BufferedCommand bF;

		while(running) {
			bF = incomingBuffer.consume();
			if(bF==null) continue;

			Server.log.produce("Processing command: " + bF.getCommandType() + " ");//TODO: add tostring() to enum class

			switch(bF.getCommandType()) {
			case DEPLOY:

				break;

			case MOVEOBJ:
				//TODO: faster to put in network manager since won't be required to propogate to this layer
				if( gameModel.isPlayerAlive(bF.getPlayerId())) 
				{	gameModel.movePlayer(bF.getPlayerId(), bF.getMove());
				sendUpdate();}
				//else ignore command
				break;
			case GAMESTATE:
				switch(bF.getGame())
				{
				case START_GAME:
					gameModel.addPlayer(bF.getPlayerId());
					if(!gameStarted)
					{
						//start the game loop
						//gameLoop = new GameLoopThread(gameModel, outgoingBuffer);
						//gameLoop.start();
						gameStarted = true;

					} //else ignore this command... //TODO: review the server logic for this
					sendUpdate();
					break;
				case END_GAME:
					break;
				case JOIN_GAME:
					break;
				case NA:
					break;
				case RESET_LIFE:
					break;
				default:
					break;
				}
				break;
			}
		}

	}

	private void sendUpdate()
	{
		//multicast the update for client
		Server.log.produce("GameLoopThread - adding '" + gameModel.toString()+ "'to OutgoingBuffer");
		outgoingBuffer.produce(gameModel.toString());//TODO: ensure integration
	}


}
