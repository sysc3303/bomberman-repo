package ca.sysc3303.bomberman.server.game.controller;

import ca.sysc3303.bomberman.buffer.OutgoingBuffer;
import ca.sysc3303.bomberman.server.Server;
import ca.sysc3303.bomberman.server.model.GameModel;

/**
 * The game loop is a thread that can be interrupted and queried against its status
 * Basic Function:
 * 1- control the game loop algorithm and performance
 * 2- Coordinate invocation of game updates unless game model specify global update API
 * 3- control sending out (instead of rendering) updates to the outgoing buffer for the network manager to send
 * 
 * @author Haydar
 *
 */
public class GameLoopThread extends Thread {

	private OutgoingBuffer outgoingBuffer;
	private int updatePerSecond;
	private boolean running;
	private GameModel gameModel;
	
	/**
	 * The period of each update is important, since the game loop algorithm
	 * @param outgoingBuffer
	 * @param updatePerSecond The frequency of updating, also determines the period of each update
	 */
	public GameLoopThread(GameModel gameModel, OutgoingBuffer outgoingBuffer, int updatePerSecond)
	{
		this.updatePerSecond = updatePerSecond;
		this.gameModel = gameModel;
		this.outgoingBuffer = outgoingBuffer;
		running = true;
	}
	
	public GameLoopThread(GameModel gameModel, OutgoingBuffer outgoingBuffer) {
		this(gameModel, outgoingBuffer, 0);
	}

	@Override
	public void run() {
		//simple loop runs forever and no control on it.
		while(running)
		{
			gameModel.update(); //global update method in game model
			//alternative: iteratve over all game objects
			//optimization: udpate specific objects
			
			//multicast the update for client
			//Server.log.produce("GameLoopThread - adding '" + gameModel.toString()+ "'to OutgoingBuffer");
			outgoingBuffer.produce(gameModel.toString());//TODO: ensure integration
		}
	}
	
}

