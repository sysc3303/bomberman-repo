package ca.sysc3303.bomberman.server.model;

import java.awt.Point;

import ca.sysc3303.bomberman.network.omar.GameSymbols;

public class Player extends MobileGameObject {
	private int id;
	private String name;
	private boolean isAlive;
	
	public Player() {
		this(GameObject.DEFAULT_POSITION, GameObject.DEFAULT_POSITION, MobileGameObject.DEFAULT_DIRECTION, null, -1);
	}
	public Player(int positionX, int positionY) {
		this(positionX, positionY, MobileGameObject.DEFAULT_DIRECTION, null, -1);
	}
	
	public Player(int positionX, int positionY, String name) {
		this(positionX, positionY, MobileGameObject.DEFAULT_DIRECTION, name, -1);
	}
	
	public Player(int positionX, int positionY, Direction direction,String name, int id) {
		super(positionX, positionY, direction);
		this.id = id;
		this.name = name;
		this.isAlive = true;
	}
	
	public boolean isAlive() {
		return isAlive;
	}
	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public int getPlayerId() {
		// TODO Auto-generated method stub
		return id;
	}
	public Point getLocation() {
		// TODO Auto-generated method stub
		return null;
	}
	
	//TODO: Add setters to MobileGameObject
	@Override
	public void setPositionX(int x)
	{
		positionX = x;
		//Going Down and pointing North
		if((positionX > x) && (super.direction == Direction.NORTH)) direction = Direction.SOUTH;
		//Going up and pointing South
		else if((positionX < x) && (super.direction == Direction.SOUTH)) direction = Direction.NORTH;
	}
	
	@Override
	public void setPositionY(int y)
	{
		positionY = y;
		//Going Right and pointing west
		if((positionY > y) && (super.direction == Direction.WEST)) direction = Direction.EAST;
		//Going Left and pointing east
		else if((positionY < y) && (super.direction == Direction.EAST)) direction = Direction.WEST;
	}
	
	@Override
	public String toString()
	{
		String s = new String("" + GameSymbols.PLAYER + GameSymbols.DELPROP
				+ getPlayerId() + GameSymbols.DELPROP + getName() + GameSymbols.DELPROP);
		switch(super.direction)
		{
			case NORTH:
				s += "n"; break;
			case SOUTH:
				s += "s"; break;
			case EAST:
				s+= "e"; break;
			case WEST:
				s += "w"; break;
			default:
				break;
		}
		return s;
	}
}
