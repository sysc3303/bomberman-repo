package ca.sysc3303.bomberman.server.model;

/**
 * Where the mobile object is pointing
 * @author Omar Samhan
 *
 */
public enum Direction {
	NORTH, SOUTH, EAST, WEST
}
