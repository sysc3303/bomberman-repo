package ca.sysc3303.bomberman.server.model;

import ca.sysc3303.bomberman.network.omar.GameSymbols;

public class EmptySpace extends StaticGameObject {

	public EmptySpace() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmptySpace(int positionX, int positionY) {
		super(positionX, positionY);
		// TODO Auto-generated constructor stub
	}
	
	public void setPositionX(int positionX)
	{
		this.positionX = positionX;
	}
	
	public void setPositionY(int positionY)
	{
		this.positionY = positionY;
	}
	
	@Override
	public String toString()
	{
		return "" + GameSymbols.EMPTY;
	}
}
