package ca.sysc3303.bomberman.server.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import ca.sysc3303.bomberman.network.omar.GameSymbols;
import ca.sysc3303.bomberman.network.omar.MoveObj;

public class GameModel implements Runnable{

	private int numplayers;
	//private Tile floor[][];
	private GameObject gameObjects [][];
	private ArrayList<Player> playersList;
	private int floorHeight;
	private int floorWidth;
	private File mapFile;
	private int actualNumPlayers;


	public GameModel(int numplayers, File mapFile)
	{
		playersList = new ArrayList<Player>();
		this.numplayers = numplayers;
		this.mapFile = mapFile;
		this.actualNumPlayers = 0;
	}

	/**
	 * Game loop to update enemies
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub

	}
	
	private Player getPlayer(int playerId)
	{
		for(Player p: playersList)
		{
			if(p.getId() == playerId) return p;
		}
		return null;
	}
	

	public synchronized void movePlayer(int playerId, MoveObj move) {
		Player p = getPlayer(playerId);
		int x = p.getPositionX(); 
		int y =	p.getPositionY();
		switch (move)
		{
		case DOWN:
			if(x!=(floorHeight-1) && //if within boundary
			!Box.class.isInstance(gameObjects[x + 1][y]) &&
			!Player.class.isInstance(gameObjects[x + 1][y])) //and the tile to move to has no object in it
			{	
				//Moving Empty Object to Player Location (UP)
				gameObjects[x + 1][y].setPositionX(x);
				
				//Moving player to Empty object (Down)
				gameObjects[x][y].setPositionX(x + 1);
				
				//Swapping Objects
				GameObject temp = gameObjects[x + 1][y];
				gameObjects[x + 1][y] = gameObjects[x][y];
				gameObjects[x][y] = temp;
			}
			break;
		case LEFT:
			if( y!=0 && //if within boundary
			!Box.class.isInstance(gameObjects[ x][ y - 1]) &&
			!Player.class.isInstance(gameObjects[ x][ y - 1]))
			{
				//Moving Empty Object to Player Location (Right)
				gameObjects[x][y-1].setPositionY(y);
				
				//Moving player to Empty object (left)
				gameObjects[x][y].setPositionY(y - 1);
				
				//Swapping Objects
				GameObject temp = gameObjects[x][y-1];
				gameObjects[x][y-1] = gameObjects[x][y];
				gameObjects[x][y] = temp;
			}
			break;
		case NA:
			break;
		case RIGHT:
			if( y!=(floorWidth-1) && //if within boundary
			!Box.class.isInstance(gameObjects[ x][ y + 1]) &&
			!Player.class.isInstance(gameObjects[ x][ y + 1]))
			{
				//Moving Empty Object to Player Location (UP)
				gameObjects[x][y+1].setPositionY(y);
				
				//Moving player to Empty object (Down)
				gameObjects[x][y].setPositionY(y + 1);
				
				//Swapping Objects
				GameObject temp = gameObjects[x][y+1];
				gameObjects[x][y+1] = gameObjects[x][y];
				gameObjects[x][y] = temp;
			}
			break;
		case UP:
			if( x!=0 && //if within boundary
			!Box.class.isInstance(gameObjects[ x - 1][ y]) &&
			!Player.class.isInstance(gameObjects[ x - 1][ y]))
			{
				//Moving Empty Object to Player Location (UP)
				gameObjects[x - 1][y].setPositionX(x);
				
				//Moving player to Empty object (Down)
				gameObjects[x][y].setPositionX(x - 1);
				
				//Swapping Objects
				GameObject temp = gameObjects[x - 1][y];
				gameObjects[x - 1][y] = gameObjects[x][y];
				gameObjects[x][y] = temp;
			}
			break;
		default:
			break;
		} //end of switch

		return; //exit because after processing one player, only one player is allowed to move at a time
	}//end of if


	/**
	 * 
	 * @param Name
	 * @return The id assigned to that player and -1 if more than 4 players wants to join
	 */
	public synchronized int addPlayer(int id)
	{
		Player P;
		//Spawning at each corner of the board clockwise
		if(id == 1) {
			
			P = new Player(0, 0, Direction.EAST, null, id);
			playersList.add(P);
			gameObjects[0][0] = P;
			return actualNumPlayers;
			
		} else if (id == 2){
			P = new Player(0, floorWidth-1, Direction.WEST, null, id);
			playersList.add(P);
			gameObjects[0][floorWidth-1] = P;
			return actualNumPlayers;
		} else if (id == 3){
			
			P = new Player(floorHeight -1, floorWidth-1, Direction.WEST, null, id);
			playersList.add(P);
			gameObjects[floorHeight -1][floorWidth-1] = P;
			return actualNumPlayers;
			
		} else if (id == 4){
			P = new Player(floorHeight -1, 0, Direction.EAST, null, id);
			playersList.add(P); 
			gameObjects[floorHeight -1][0] = P;
			return actualNumPlayers;
			
		} else return -1;
	}

	public synchronized boolean removePlayer(int playerId)
	{
		for (Player p: playersList)
			if (p.getPlayerId() == playerId) return true;
		return false;
	}

	//ACCESSORS

	public synchronized int getNumplayers() {
		return numplayers;
	}

	public synchronized void setNumplayers(int numplayers) {
		this.numplayers = numplayers;
	}

	/**
	 * FIXME: HUGE potential BUG here, rather return a state, and throw exception
	 * possible return values from different method calls, 1 retrieve player, 2 get status
	 * What if player is not found?!
	 * 
	 * @param playerId
	 * @return Currently if player is not found then return false
	 */
	public synchronized boolean isPlayerAlive(int playerId) {
		for(Player p : playersList)
			if(p.getPlayerId() == playerId)
				return p.isAlive();
		return false;
	}

	public void update() {
		// TODO Auto-generated method stub

	}
	
	private void initMap() throws IOException
	{
		BufferedReader bR = new BufferedReader(new FileReader(mapFile));
		StringTokenizer sT1;
		int height,width;
		height = width = 0;
		boolean flag = true;
		String row;
		while((row=bR.readLine())!=null)
		{
			if(flag)
			{
				sT1 = new StringTokenizer(row, ""+GameSymbols.DELTILE);
				while(sT1.hasMoreTokens())
				{
					sT1.nextToken();
					width++;				
				}//Read Column
				flag = false;
			}
			height++;
		}//Read Row
		
		floorWidth = width;
		floorHeight = height;
		
		gameObjects = new GameObject [floorWidth][floorHeight];
		
		bR.close();
	}
	
	
	public synchronized void initGameModel() throws IOException
	{
		BufferedReader bR = new BufferedReader(new FileReader(mapFile));
		StringTokenizer sT1;
		
		String row;
		
		//Initializing Map...
		initMap();
		
		for(int i = 0; (row=bR.readLine())!=null; i++)
		{
			sT1 = new StringTokenizer(row, ""+GameSymbols.DELTILE);
			//To initialize tiles in a row
			for(int j = 0; sT1.hasMoreTokens(); j++)
			{
				String tile = sT1.nextToken();
				initGameObject(tile, i, j);
			
			}//Read Column
		}//Read Row
		
		bR.close();
	}
	
	private void initGameObject(String s, int x, int y)
	{	
		//To initialize properties or other (power ups inside a box)
		if(s.length() == 1)
		{
			if(s.equals("b")){
				gameObjects[x][y] = new Box(x,y);
			} else if(s.equals("e")) {
				gameObjects[x][y] = new EmptySpace(x,y);
			} else if(s.equals("u")) {
				gameObjects[x][y] = new PowerUp(x,y);
			} else {
				gameObjects[x][y] = null;
			}
		}
		
		StringTokenizer sT2 = new StringTokenizer(s, ""+GameSymbols.DELPROP);
		if(!sT2.hasMoreTokens()){
			
		}
	}
	
	public synchronized int getActualNumPlayers() {
		return actualNumPlayers;
	}

	@Override
	public synchronized String toString()
	{
		String s = new String();
		for(int i=0; i < floorHeight;i++)
		{
			for(int j=0; j < floorWidth; j++)
			{
				s += "" + gameObjects[i][j].toString() + GameSymbols.DELTILE;
			}
			s += "" + GameSymbols.DELROW;
		}
		return s + GameSymbols.DELEND;
	}
	
	public static void main(String argv[])
	{
		File f = new File("resources/maps/map.txt");
		GameModel g = new GameModel(4, f);
		try {
			g.initGameModel();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		@SuppressWarnings("unused")
		String s = g.toString();
		int p1;
		g.addPlayer(p1 = 1);
		s = g.toString();
		g.movePlayer(p1, MoveObj.DOWN);
		s = g.toString();
		g.movePlayer(p1, MoveObj.DOWN);
		s = g.toString();
		g.movePlayer(p1, MoveObj.DOWN);
		s = g.toString();
		g.movePlayer(p1, MoveObj.DOWN);
		s = g.toString();
		g.movePlayer(p1, MoveObj.DOWN);
		s = g.toString();
		g.movePlayer(p1, MoveObj.DOWN);
		s = g.toString();
		g.movePlayer(p1, MoveObj.DOWN);
		s = g.toString();
		g.movePlayer(p1, MoveObj.DOWN);
		s = g.toString();
	}
}
