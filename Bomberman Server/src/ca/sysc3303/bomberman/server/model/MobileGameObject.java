package ca.sysc3303.bomberman.server.model;

public abstract class MobileGameObject extends GameObject {
	
	protected Direction direction;
	protected static final Direction DEFAULT_DIRECTION = Direction.SOUTH;

	public MobileGameObject() {
		this(GameObject.DEFAULT_POSITION, GameObject.DEFAULT_POSITION, DEFAULT_DIRECTION);
	}

	public MobileGameObject(int positionX, int positionY) {
		this(positionX, positionY, DEFAULT_DIRECTION);
	}
	
	public MobileGameObject(int positionX, int positionY, Direction direction) {
		super(positionX, positionY);
		this.direction = direction;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}
}
