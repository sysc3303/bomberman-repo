package ca.sysc3303.bomberman.server.model;

import ca.sysc3303.bomberman.network.omar.GameSymbols;

public class PowerUp extends StaticGameObject {
	
	public PowerUp() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PowerUp(int positionX, int positionY) {
		super(positionX, positionY);
		// TODO Auto-generated constructor stub
	}

	//TODO: Add type of PowerUp after GameSymbols.POWERUP
	@Override
	public String toString()
	{
		return "" + GameSymbols.POWERUP;
	}
}
