package ca.sysc3303.bomberman.server.model;

import java.util.ArrayList;

import ca.sysc3303.bomberman.network.omar.GameSymbols;

//account for fx effects: i.e. explosion effect
public class Tile {
	private MobileGameObject mobileGameObject;
	private ArrayList<StaticGameObject> staticGameObjects;
	
	public Tile(MobileGameObject mobileGameObject, ArrayList<StaticGameObject> staticGameObjects) {
		super();
		this.mobileGameObject = mobileGameObject;
		this.staticGameObjects = staticGameObjects;
	}
	
	public Tile() 
	{
		staticGameObjects = new ArrayList<StaticGameObject>();
	}
	
	public Tile(MobileGameObject mobileGameObject) {
		this();
		this.mobileGameObject = mobileGameObject;
	}
	
	public Tile(StaticGameObject staticGameObject) {
		this();
		this.staticGameObjects.add(staticGameObject);
	}
	
	public Player containsPlayer()
	{
		if(Player.class.isInstance(mobileGameObject)) return (Player)mobileGameObject;
		else return null;
	}
	
	public boolean containsBox()
	{
		for(StaticGameObject s: staticGameObjects)
			if(Box.class.isInstance(s)) return true;
		return false;
	}
	
	public boolean containsPowerUp()
	{
		for(StaticGameObject s: staticGameObjects)
			if(PowerUp.class.isInstance(s)) return true;
		return false;
	}
	
	public GameObject getGameObject()
	{
		if(mobileGameObject != null) return mobileGameObject;
		for(StaticGameObject s: staticGameObjects)
		{
			if(Box.class.isInstance(s)) return s; //Box should have a priority for the ModelObject game representation
			else if(s != null) return s;
		}
		return null;		
	}
	
	//**********FOR TESTING************/////
	public static void main(String []argv)
	{
		Tile t = new Tile(new Box());
		
		if(t.containsPlayer() != null) System.out.println("Success!");
		else System.out.println("Failed :(");
	}

	public MobileGameObject getMobileGameObject() {
		return mobileGameObject;
	}

	public ArrayList<StaticGameObject> getStaticGameObjects() {
		return staticGameObjects;
	}

	public void removeMobileObject() {
		mobileGameObject = null;
	}
	
	@Override
	public String toString()
	{
		return "" + getGameObject().toString() + GameSymbols.DELTILE;
	}
	
}
