package ca.sysc3303.bomberman.server.model;

public abstract class GameObject {
	protected int positionX;
	protected int positionY;
	protected static final int DEFAULT_POSITION = -1;
	
	
	public GameObject(int positionX, int positionY)
	{
		this.positionX = positionX;
		this.positionY = positionY;
	}
	
	/**
	 * Sets the positions to -1 by default. NOT recommended!
	 */
	public GameObject()
	{
		this(DEFAULT_POSITION,DEFAULT_POSITION);
	}

	public int getPositionX() {
		return positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}
}
