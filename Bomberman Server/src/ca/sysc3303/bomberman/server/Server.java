package ca.sysc3303.bomberman.server;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import ca.sysc3303.bomberman.buffer.IncomingBuffer;
import ca.sysc3303.bomberman.buffer.OutgoingBuffer;
import ca.sysc3303.bomberman.server.game.controller.GameController;
import ca.sysc3303.bomberman.server.networking.NetworkManager;


public class Server {
	
	/** Tutorial: http://www.vogella.com/tutorials/Logging/article.html */
	public static LogBuffer log;
	
	/**
	 * @param args[0] IP
	 * args[1] port
	 * args[2] map file path
	 */
	public static void main(String[] args) {

		Random r = new Random();
		log = LogBuffer.getInstance();
		LoggerThread logger = new LoggerThread();
		logger.start();
				
		Integer port;
		File mapFile;
		
		//Subsystems
		GameController gameController;
		NetworkManager networkManager;
		
		port = 5000;
		int x = r.nextInt()%2;
		if(x == 0) mapFile = new File ("resources/maps/map1.txt"); //MILESTONE 2: change/add cmd line arg
		else  mapFile = new File ("resources/maps/map2.txt"); //MILESTONE 2: change/add cmd line arg
		
		log.produce("Creating NetworkManager with port: " + port);
		networkManager = new NetworkManager(port, IncomingBuffer.getInstance(), OutgoingBuffer.getInstance());//TODO: integrate with buffers
		log.produce("Creating GameController");
		gameController = new GameController(IncomingBuffer.getInstance(), 
				OutgoingBuffer.getInstance(),
				mapFile
			);
		
		
	
		//step 1: setup networking
		networkManager.setup();//starts networking threads
		//step 2: initialize game model
		(new Thread(gameController)).start();
		
	}
}
