package ca.sysc3303.bomberman.server;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggerThread extends Thread{
	private LogBuffer buffer;
	private Logger logger;
	
	public LoggerThread(){
		this.buffer = LogBuffer.getInstance();
		
		this.logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	    try {
	    	FileHandler fileText = new FileHandler("Log.txt");
	    	fileText.setFormatter(new SimpleFormatter());
	    	this.logger.addHandler(fileText);	    	
		} catch (SecurityException e) {
			System.err.println("Logger set up error: security Exception");			
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Logger set up error: I/O Exception");
			e.printStackTrace();
		}
	}
	
	public void run(){
		String data;
		while(true){
			data = buffer.consume();
			this.logger.info(data);
		}
	}
	
	
}