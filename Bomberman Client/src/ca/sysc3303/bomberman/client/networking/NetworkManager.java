package ca.sysc3303.bomberman.client.networking;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.*;

import ca.sysc3303.bomberman.buffer.IncomingBuffer;
import ca.sysc3303.bomberman.buffer.OutgoingBuffer;
import ca.sysc3303.bomberman.client.GUI.View;
import ca.sysc3303.bomberman.network.omar.BufferedCommand;
import ca.sysc3303.bomberman.network.omar.GameState;
import ca.sysc3303.bomberman.network.omar.MoveObj;

public class NetworkManager {

	private View view;
	private InetAddress serverAddr;
	private int incomingPort;
	private int serverPort;
	private DatagramSocket incomingSocket;
	private DatagramSocket outgoingSocket;
	private UserOutputThread outThread;
	private UserInputThread inThread;

	public NetworkManager(String serverIP, int serverPort, View view)
	{
		this.view = view;

		try {
			serverAddr = InetAddress.getByName(serverIP);
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.out.println("couldn't resolve IP address");
		}

		this.serverPort = serverPort;

		try {
			this.outgoingSocket = new DatagramSocket();
		} catch (SocketException e) {
			System.exit(1);
		}
		
		this.incomingPort = 5001;
		
		try {
			this.incomingSocket = new DatagramSocket(this.incomingPort);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	public void setup(){
		inThread = new UserInputThread();
		outThread = new UserOutputThread();
		
		inThread.start();
		outThread.start();
	}

	
	/*
	 * This thread takes input from the IncomingBuffer (filled with user commands),
	 * serializes them and sends them to the server on the outgoingPort
	 */
	private class UserInputThread extends Thread{

		private IncomingBuffer inBuffer;

		public UserInputThread(){
			inBuffer = IncomingBuffer.getInstance();
		}

		@Override 
		public void run(){
			
			byte[] sendData = new byte[1024];
			BufferedCommand command = null;
			DatagramPacket sendPacket = null;
			
			while(true){
				command = inBuffer.consume();
				
				//testBuffer
				printCommand(command);
				
				sendPacket = buildPacket(command);				

				try {
					outgoingSocket.send(sendPacket);
					System.out.println("Sending packet");
				} catch (IOException e) {
					System.out.println("Error - Couldn't send packet");
				}
			}
		}

		private DatagramPacket buildPacket(BufferedCommand command){
			ByteArrayOutputStream b = null;
			ObjectOutputStream o = null;
			byte [] sendData = null;

			//serialize command
			b = new ByteArrayOutputStream();
			
			try {
				o = new ObjectOutputStream(b);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			try {
				o.writeObject(command);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			sendData = b.toByteArray();

			DatagramPacket sendPacket = new DatagramPacket(sendData,sendData.length,serverAddr,serverPort);
			return sendPacket;
		}

		//used to test buffer
		private void printCommand(BufferedCommand command){
			int playerID = command.getPlayerId();
			MoveObj move = command.getMove();
			GameState gameState = command.getGame();
			String moveString = null;
			String gameStateString = null;
			
			if (move == MoveObj.UP){
				moveString = "Up";
			} else if (move == MoveObj.DOWN){
				moveString = "Down";
			} else if (move == MoveObj.LEFT){
				moveString = "Left";
			} else if (move == MoveObj.RIGHT){
				moveString = "Right";
			} else {
				moveString = "None";
			}
			
			if (gameState == GameState.START_GAME){
				gameStateString = "Start";
			} else if (gameState == GameState.END_GAME){
				gameStateString = "End";
			} else if (gameState == GameState.JOIN_GAME){
				gameStateString = "Join";
			} else if (gameState == GameState.RESET_LIFE){
				gameStateString = "Reset Life";
			} else if (gameState == GameState.NA){
				gameStateString = "NA";
			} else {
				gameStateString = "None";
			}
			
			System.out.println("playerID = " + Integer.toString(playerID) + ", Move = "  + moveString +
					", GameState = " + gameStateString);
			
			
		}
	}

	/*
	 * This thread receives the GUI refresh data from the server in the form of a packet.
	 * Casts the packet data from byte[] to String and places the String in the OugoingBuffer for the View 
	 */
	private class UserOutputThread extends Thread{

		private OutgoingBuffer outBuffer;
	
		public UserOutputThread(){
			outBuffer = OutgoingBuffer.getInstance();
		}
		
		@Override
		public void run(){
			
			byte[] buf = new byte[1024];
			DatagramPacket recvPacket = new DatagramPacket(buf, buf.length);
			String recvData = new String();
			
			while(true){
				
				System.out.println("waiting to receive a packet");
				
				try {
					incomingSocket.receive(recvPacket);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				System.out.println("received a packet");
				
				recvData = new String(recvPacket.getData() );
				char[] data = recvData.toCharArray();
				String s = "";
				for(int i =0; i < recvData.length() ; i++)
				{
					if(data[i] == '@')
					{
						break;
					} else {
						s += data[i];
					}
				}
				System.out.println(s);
				outBuffer.produce(s);
			}
		}
	}
}

