package ca.sysc3303.bomberman.client;
import java.io.*;
import java.util.Locale;
import java.util.Scanner;

import ca.sysc3303.bomberman.buffer.IncomingBuffer;
import ca.sysc3303.bomberman.network.omar.BufferedCommand;
import ca.sysc3303.bomberman.network.omar.GameState;
import ca.sysc3303.bomberman.network.omar.MoveObj;

public class TestDriver implements Runnable {

	private IncomingBuffer inBuffer = IncomingBuffer.getInstance();
	private String textFilePath;

	public TestDriver(String textFilePath)
	{
		this.textFilePath = textFilePath;
	}

	@Override
	public void run() {
		Scanner s = null;
		String testSequence = null;
		System.out.println(s);
		try {
			s = new Scanner(new File(this.textFilePath));
			s.useLocale(Locale.US);
		} catch (FileNotFoundException e) {
			System.out.println("file i/o exception");
			e.printStackTrace();
		}
		while(s.hasNext()){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			testSequence = s.next();
			BufferedCommand bufferCommand = parseCommand(testSequence);
			inBuffer.produce(bufferCommand);
		}
	}
	
	private BufferedCommand parseCommand(String testSequence)
	{
		BufferedCommand bufferCommand = null; 
		String delims = "[,]";
		String[] tokens = testSequence.split(delims);

		if(tokens[0].equals("G"))
		{
			if (tokens[1].equals("START"))
			{
				bufferCommand = new BufferedCommand(Integer.parseInt(tokens[2]),GameState.START_GAME);
			}
			else if(tokens[1].equals("END"))
				bufferCommand = new BufferedCommand(Integer.parseInt(tokens[2]),GameState.END_GAME);
		}
		else if (tokens[0].equals("M"))
		{
			if(tokens[1].equals("UP"))
			{
				bufferCommand = new BufferedCommand(Integer.parseInt(tokens[2]),MoveObj.UP);
			}
			else if (tokens[1].equals("DOWN"))
			{
				bufferCommand = new BufferedCommand(Integer.parseInt(tokens[2]),MoveObj.DOWN);
			}
			else if(tokens[1].equals("LEFT"))
			{
				bufferCommand = new BufferedCommand(Integer.parseInt(tokens[2]),MoveObj.LEFT);
			}
			else
			{
				bufferCommand = new BufferedCommand(Integer.parseInt(tokens[2]),MoveObj.RIGHT);
			}
		}
		return bufferCommand;
	}
	
}