package ca.sysc3303.bomberman.client.GUI;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ca.sysc3303.bomberman.buffer.OutgoingBuffer;
import ca.sysc3303.bomberman.network.omar.GameSymbols;

public class View extends JPanel implements Runnable{

	static int WIDTH = 5;
	static int HEIGHT = 5;
	private OutgoingBuffer buffer;
	private JFrame frame;
	private JLabel[][] gameState; //shows the board along with context
	private boolean isGridSizeSet;
	private Image image;
	private ImageIcon imageIcon;
	private Image boxImage;
	private ImageIcon boxImageIcon;
	public View() {
		super();
		buffer = OutgoingBuffer.getInstance();
		frame = new JFrame("Bomberman Client - v1.1");
		try {
			image = ImageIO.read(new File("Resources/images/bomberman.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			boxImage = ImageIO.read(new File("Resources/images/box.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		boxImageIcon =null;
		imageIcon = null;
		frame.setContentPane(this);
		setLayout(new GridLayout(5,5));
		this.setPreferredSize(new Dimension(500,500));

		gameState = new JLabel[HEIGHT][WIDTH];
		for(int i = 0; i < WIDTH; i++)
		{
			for(int j = 0; j < HEIGHT; j++)
			{
				add(gameState[i][j] = new JLabel());
			}
		}

		isGridSizeSet = false;
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.pack();
		frame.setLocationRelativeTo(null);
	}

	@Override
	public void run() {


		while(true) {

			parse(buffer.consume());
			System.out.println("View Update happened!");
		}
	}

	public static void main(String args[])
	{
		View view = new View();
		view.parse("p,1;e;e;e;p,2$e;b;p,3;e;e$b;b;e;e;e$e;b;p,3;e;e$e;e;b;b;e$");
	}


	private void parse(String data)
	{
		StringTokenizer sT1, sT2, sT3;
		int x = 0;
		int y = 0;
		String character;
		imageIcon = null;
		boxImageIcon =null;
		sT1 = new StringTokenizer(data, ""+GameSymbols.DELROW);

		while(sT1.hasMoreTokens()) //while i have more rows to parse
		{
			sT2 = new StringTokenizer(sT1.nextToken(), ""+GameSymbols.DELTILE);
			y = 0;
			while(sT2.hasMoreTokens()) //while i have more tile to parse
			{
				//				sT3 = new StringTokenizer(sT2.nextToken(), ""+GameSymbols.DELPROP);
				//				while(sT3.hasMoreTokens()) 
				//				{
				//					
				//					character = sT3.nextToken();
				//					if("p".equals(character)){
				//						gameState[x][y].setText("p" + sT3.nextToken());
				//					}
				//				}
				String [] tokens = sT2.nextToken().split("[,]");
				if("p".equals(tokens[0])) gameState[x][y].setIcon(imageIcon = new ImageIcon(image));
				else if("b".equals(tokens[0]))gameState[x][y].setIcon(boxImageIcon = new ImageIcon(boxImage));//gameState[x][y].setText("b");
				else if ("@".equals(tokens[0]))return;
				//				else gameState[x][y].setText("e");
				else 
				{
					gameState[x][y].setIcon(null);
					//gameState[x][y].setText("e");
				}
				y++;
			}//end of 
			x++;
		}//end of row 
	}
}

class Tile extends JPanel{

	int width, height;
	JLabel image;

	public Tile(Image image)
	{
		//this.width = width;
		//this.height = height;
		this.setMaximumSize(new Dimension(width, height));
		this.setMinimumSize(new Dimension(width, height));
		this.setLayout(null);
		this.image = new JLabel(new ImageIcon(image));
		add(this.image);
	}


}