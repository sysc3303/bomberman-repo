package ca.sysc3303.bomberman.client;

import ca.sysc3303.bomberman.buffer.IncomingBuffer;
import ca.sysc3303.bomberman.buffer.OutgoingBuffer;
import ca.sysc3303.bomberman.client.GUI.View;
import ca.sysc3303.bomberman.client.networking.NetworkManager;

//testingBuffer
import ca.sysc3303.bomberman.network.omar.*;

public class Client {

	/**
	 * @param args[0] IP
	 * args[1] choose test case (either 1,2 or 3) Please check Readme.txt
	 */
	public static void main(String[] args) {

		String serverIP;
		Integer serverPort;
		NetworkManager networkManager;
		View view = null;
		TestDriver testDriver;

		//get arguments
		serverIP = args[0];
		serverPort = 5000;

		System.out.println("Starting TestDriver...");
		if(Integer.parseInt(args[1]) == 1)
		{
			testDriver = new TestDriver("Resources/testcase1.txt");
			(new Thread(testDriver)).start();
		}
		else if (Integer.parseInt(args[1])==2)
		{
			testDriver = new TestDriver("Resources/testcase2.txt");
			(new Thread(testDriver)).start();			
			
		}
		else if (Integer.parseInt(args[1])==3)
		{
			testDriver = new TestDriver("Resources/testcase3.txt");
			(new Thread(testDriver)).start();		}
		System.out.println("Starting NetworkManager with IP = " + serverIP + ", Port = " + Integer.toString(serverPort));
		networkManager = new NetworkManager(serverIP, serverPort, view);
		networkManager.setup();

		view = new View();

		(new Thread(view)).start();
	}
}
